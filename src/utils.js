export function move(array, element, delta) {
  const index = array.indexOf(element)
  const newIndex = index + delta

  if (
    newIndex < 0  ||
    newIndex == array.length
  ) {
    return
  }

  const indexes = [index, newIndex].sort()
  array.splice(indexes[0], 2, array[indexes[1]], array[indexes[0]])
}