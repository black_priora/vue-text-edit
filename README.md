# my-component

> A Vue.js component

# Usage

## Installation

### Using yarn

`yarn add my-component`

### Using npm

`npm i --save my-component`

## License

This project is licensed under [MIT License](http://en.wikipedia.org/wiki/MIT_License)
